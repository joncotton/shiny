//
//  ViewController.swift
//  shiny
//
//  Created by Cotton, Jonathan (Mobile Developer) on 06/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit
import SceneKit
import OpenGLES

class ViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var backgroundView: GlassBackgroundView!
//    @IBOutlet var glassViews: [GlassView]!
    var didSetupLightSources = false
    let lightPoint = CGPoint(x: 160.0, y: 180.0)
    //let openGLContext = EAGLContext(API: .OpenGLES2)
//    @IBOutlet weak var sceneView: SCNView!
    
    @IBOutlet weak var scrollView: UIScrollView!
//    let rotate90AboutZ = SCNAction.rotateByX(0.0, y: 0.0, z: CGFloat(M_PI_2), duration: 0.0)
    let omniLightNode = SCNNode()
//
//    var currentAngle: Float = 0.0
//    var coinNode = SCNNode()
//    var squareNode = SCNNode()
//    
    override func viewDidLoad() {
//        super.viewDidLoad()
//        sceneView.userInteractionEnabled = true
//        sceneView.autoenablesDefaultLighting = true
        scrollView.delegate = self
        backgroundView.clipsToBounds = false
        scrollView.clipsToBounds = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        //coinSceneSetup()
        lightSetup()
        backgroundView.addTileWithRect(CGRectMake(0,0,9999,9999))
    }
    
    func lightSetup() {
//        omniLightNode.light = SCNLight()
//        omniLightNode.light!.type = SCNLightTypeOmni
//        omniLightNode.light!.color = UIColor(white: 0.75, alpha: 1.0)
//        omniLightNode.position = SCNVector3Make(0, 0, 20)
        
//        for glassView in glassViews {
//            glassView.scene?.rootNode.addChildNode(omniLightNode)
//        }
        
//        let panRecognizer = UIPanGestureRecognizer(target: self, action: "panGesture:")
//        view.addGestureRecognizer(panRecognizer)
//
//        let tapRecognizer = UITapGestureRecognizer(target: self, action: "tapGesture:")
//        view.addGestureRecognizer(tapRecognizer)
    }
//    
//    func coinSceneSetup() {
//        
//        let coinScene = SCNScene()
//        coinScene.background.contents = UIColor.whiteColor()
//        //coin is rendered as a cylinder with a very small height
////        let coinGeometry = SCNCylinder(radius: 50, height: 2)
////        coinNode = SCNNode(geometry: coinGeometry)
////        coinNode.position = SCNVector3Make(0.0, 25.0, 25.0)
////        coinScene.rootNode.addChildNode(coinNode)
////        //rotate coin 90 degrees about the z axis so that it stands upright
////        coinNode.runAction(rotate90AboutZ)
//        
////        let path = UIBezierPath(rect: CGRectMake(0.0, 0.0, 100.0, 100.0))
////        path.fill()
////        path.flatness = 1.0
////        let square = SCNShape(path: path, extrusionDepth: 2.0)
//        let square = SCNPlane(width: 5.0, height: 5.0)
//        square.cornerRadius = 0.75
////        let square = SCNBox(width: 5.0, height: 5.0, length: 0.1, chamferRadius: 2.0)
//        squareNode = SCNNode(geometry: square)
////        squareNode.scale = SCNVector3Make(0.0, 5.0, 5.0)
//        //squareNode.position = SCNVector3Make(0.0, 0.0, 100.0)
////        squareNode.rotation = SCNVector4Make(0, 1.0, 0, Float(M_PI/4.0));
//        coinScene.rootNode.addChildNode(squareNode)
//        
//        // camera
//        let cameraNode = SCNNode()
//        cameraNode.camera = SCNCamera()
//        cameraNode.position = SCNVector3Make(0, 0, 2.0)
////        cameraNode.rotation = SCNVector4Make(1, 0, 0, -atan2(10.0, 20.0))
//        coinScene.rootNode.addChildNode(cameraNode)
//        
////        let ambientLightNode = SCNNode()
////        ambientLightNode.light = SCNLight()
////        ambientLightNode.light!.type = SCNLightTypeAmbient
////        ambientLightNode.light!.color = UIColor(white: 0.67, alpha: 1.0)
////        coinScene.rootNode.addChildNode(ambientLightNode)
//        
////        let omniLightNode = SCNNode()
//        omniLightNode.light = SCNLight()
//        omniLightNode.light!.type = SCNLightTypeOmni
//        omniLightNode.light!.color = UIColor(white: 0.75, alpha: 1.0)
//        omniLightNode.position = SCNVector3Make(0, 0, 20)
//        coinScene.rootNode.addChildNode(omniLightNode)
//        
//        let shinyCoinMaterial = SCNMaterial()
//        shinyCoinMaterial.diffuse.contents = UIColor(red: 0.8, green: 0.8, blue: 1.0, alpha: 1.0)
//        shinyCoinMaterial.specular.contents = UIColor.whiteColor()
////        shinyCoinMaterial.transparent.contents = UIImage(named:"glassMap")!
//        shinyCoinMaterial.transparency = 0.8
//        shinyCoinMaterial.transparencyMode = .RGBZero
//        shinyCoinMaterial.shininess = 1.0
//        shinyCoinMaterial.fresnelExponent = 0.9
//        shinyCoinMaterial.litPerPixel = true
//        square.firstMaterial = shinyCoinMaterial
//        
////        sceneView.scene = coinScene
////        let panRecognizer = UIPanGestureRecognizer(target: self, action: "panGesture:")
////        sceneView.addGestureRecognizer(panRecognizer)
////        
////        let tapRecognizer = UITapGestureRecognizer(target: self, action: "tapGesture:")
////        sceneView.addGestureRecognizer(tapRecognizer)
//    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !didSetupLightSources {
//            let center = CGPoint(x: CGRectGetMidX(view.bounds), y: CGRectGetMidY(view.bounds))
            
//            for glassView in glassViews {
//                glassView.eaglContext = openGLContext
//                setLightSourcePositionForGlassView(glassView, toPoint: lightPoint)
//            }
//            
            setLightSourcePositionForBackgroundGlassView(backgroundView, toPoint: lightPoint)
            
            didSetupLightSources = true
        }
    }
    
//    //allows for coin to spin with a right or left finger swipe, while still keeping it rotated 90 degrees about z axis
    func panGesture(sender: UIPanGestureRecognizer) {
        let translation = sender.translationInView(sender.view!)
//        var newAngle = (Float)(translation.x)*(Float)(M_PI)/180.0
//        newAngle += currentAngle
//        squareNode.runAction(SCNAction.rotateToX(CGFloat(newAngle), y: 0.0, z: CGFloat(M_PI_2), duration: 0.0))
//        omniLightNode.runAction(SCNAction.moveByX(CGFloat(translation.x/4), y: -CGFloat(translation.y/4), z: 0.0, duration: 0.0))
//        if(sender.state == UIGestureRecognizerState.Ended) {
//            currentAngle = newAngle
//        }
        
//        for glassView in glassViews {
//            glassView.omniLightNode.runAction(SCNAction.moveByX(CGFloat(translation.x/4), y: -CGFloat(translation.y/4), z: 0.0, duration: 0.0))
//        }
    }
    
    func tapGesture(sender: UITapGestureRecognizer) {
//        for glassView in glassViews {
//            let point = sender.locationInView(view)
//            setLightSourcePositionForGlassView(glassView, toPoint: point)
//        }
    }
    
    func setLightSourcePositionForGlassView(glassView: GlassView, toPoint point: CGPoint) {
        let centerX = CGRectGetMidX(glassView.frame)
        let centerY = CGRectGetMidY(glassView.frame)
        
        var xPos = centerX - point.x
        var yPos = centerY - point.y
        
        if point.x < centerX {
            xPos = -abs(xPos)
        } else {
            xPos = abs(xPos)
        }
        
        if point.y > centerY {
            yPos = -abs(yPos)
        } else {
            yPos = abs(yPos)
        }
        
        let reductionFactor = CGFloat(4)
        xPos = xPos / reductionFactor
        yPos = yPos / reductionFactor
            
        glassView.omniLightNode.position = SCNVector3Make(Float(xPos), Float(yPos), 20)
    }
    
    func setLightSourcePositionForBackgroundGlassView(glassView: GlassBackgroundView, toPoint point: CGPoint) {
        let centerX = CGRectGetMidX(glassView.frame)
        let centerY = CGRectGetMidY(glassView.frame)
        
        var xPos = centerX - point.x
        var yPos = centerY - point.y
        
        if point.x < centerX {
            xPos = -abs(xPos)
        } else {
            xPos = abs(xPos)
        }
        
        if point.y > centerY {
            yPos = -abs(yPos)
        } else {
            yPos = abs(yPos)
        }
        
        let reductionFactor = CGFloat(10)
        xPos = xPos / reductionFactor
        yPos = yPos / reductionFactor
        
        glassView.omniLightNode.position = SCNVector3Make(Float(xPos), Float(yPos), 20)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let dy = scrollView.contentOffset.y
        let dx = scrollView.contentOffset.x
        let newLightPoint = CGPoint(x: lightPoint.x + dx, y: lightPoint.y + dy)
//        for glassView in glassViews {
//            setLightSourcePositionForGlassView(glassView, toPoint: newLightPoint)
//        }
        setLightSourcePositionForBackgroundGlassView(backgroundView, toPoint: newLightPoint)
    }
}
