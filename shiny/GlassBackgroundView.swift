//
//  GlassBackgroundView.swift
//  shiny
//
//  Created by Cotton, Jonathan (Mobile Developer) on 06/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit
import SceneKit

class GlassBackgroundView: SCNView {
    let omniLightNode = SCNNode()
    
    lazy var glassMaterial: SCNMaterial = {
        let glassMaterial = SCNMaterial()
        glassMaterial.diffuse.contents = UIColor.whiteColor()
        glassMaterial.specular.contents = UIColor.whiteColor()
        glassMaterial.transparency = 0.8
        glassMaterial.transparencyMode = .RGBZero
        glassMaterial.shininess = 1.0
        glassMaterial.litPerPixel = true
        
        return glassMaterial
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sceneSetup()
        addMask()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sceneSetup()
        addMask()
    }

    func sceneSetup() {
        backgroundColor = UIColor.clearColor()
        let scene = SCNScene()
        
        // camera
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3Make(0, 0, 100.0)
        scene.rootNode.addChildNode(cameraNode)

        // light source
        omniLightNode.light = SCNLight()
        omniLightNode.light!.type = SCNLightTypeSpot
        omniLightNode.light!.color = UIColor(white: 0.55, alpha: 1.0)
        omniLightNode.position = SCNVector3Make(0, 0, 10)
        scene.rootNode.addChildNode(omniLightNode)
        
        self.scene = scene
        showsStatistics = true
    }

    func addTileWithRect(rect: CGRect) {
        let point = rect.origin
        let centerX = CGRectGetMidX(bounds)
        let centerY = CGRectGetMidY(bounds)
        
        var xPos = centerX - point.x
        var yPos = centerY - point.y
        
        if point.x < centerX {
            xPos = -abs(xPos)
        } else {
            xPos = abs(xPos)
        }
        
        if point.y > centerY {
            yPos = -abs(yPos)
        } else {
            yPos = abs(yPos)
        }
        
        let reductionFactor = CGFloat(4)
        xPos = xPos / reductionFactor
        yPos = yPos / reductionFactor
        let width = rect.size.width / reductionFactor
        let height = rect.size.height / reductionFactor
        
        let square = SCNPlane(width: width, height: height)
        square.cornerRadius = 0.75
        let squareNode = SCNNode(geometry: square)
        square.firstMaterial = glassMaterial
        
        scene?.rootNode.addChildNode(squareNode)
    }
    
    func addMask() {
        let maskLayer = CALayer()
        var currentY = CGFloat(0)
        var currentX = CGFloat(0)
        
        for i in 0 ..< 30 {
            if i % 3 == 0 {
                currentY++
                currentX = 0
            }
            
            let rect = CGRectMake(currentX * 104, currentY * 104, 100, 100)
            let path = CGPathCreateWithRect(rect, nil)
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = path
            shapeLayer.fillColor = UIColor.blackColor().colorWithAlphaComponent(1.0).CGColor
            
            maskLayer.addSublayer(shapeLayer)
            
            addTile(rect)
            
            currentX++
        }
        
        layer.mask = maskLayer
    }
    
    func addTile(rect: CGRect) {
        let frame = CGRectInset(rect, 4.0, 4.0)
        
        let tile = UIView(frame: frame)
        tile.backgroundColor = UIColor.lightGrayColor()
        addSubview(tile)
    }
}
