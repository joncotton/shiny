//
//  GlassView.swift
//  shiny
//
//  Created by Cotton, Jonathan (Mobile Developer) on 06/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit
import SceneKit

class GlassView: SCNView {
    let omniLightNode = SCNNode()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sceneSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sceneSetup()
    }
    
    func sceneSetup() {
        backgroundColor = UIColor.clearColor()
        let coinScene = SCNScene()
//        coinScene.background.contents = UIColor.whiteColor()
        //coin is rendered as a cylinder with a very small height
        //        let coinGeometry = SCNCylinder(radius: 50, height: 2)
        //        coinNode = SCNNode(geometry: coinGeometry)
        //        coinNode.position = SCNVector3Make(0.0, 25.0, 25.0)
        //        coinScene.rootNode.addChildNode(coinNode)
        //        //rotate coin 90 degrees about the z axis so that it stands upright
        //        coinNode.runAction(rotate90AboutZ)
        
        //        let path = UIBezierPath(rect: CGRectMake(0.0, 0.0, 100.0, 100.0))
        //        path.fill()
        //        path.flatness = 1.0
        //        let square = SCNShape(path: path, extrusionDepth: 2.0)
        let square = SCNPlane(width: 5.0, height: 5.0)
        square.cornerRadius = 0.75
        //        let square = SCNBox(width: 5.0, height: 5.0, length: 0.1, chamferRadius: 2.0)
        let squareNode = SCNNode(geometry: square)
        //        squareNode.scale = SCNVector3Make(0.0, 5.0, 5.0)
        //squareNode.position = SCNVector3Make(0.0, 0.0, 100.0)
        //        squareNode.rotation = SCNVector4Make(0, 1.0, 0, Float(M_PI/4.0));
        coinScene.rootNode.addChildNode(squareNode)
        
        // camera
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3Make(0, 0, 2.0)
        //        cameraNode.rotation = SCNVector4Make(1, 0, 0, -atan2(10.0, 20.0))
        coinScene.rootNode.addChildNode(cameraNode)
        
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = SCNLightTypeAmbient
        ambientLightNode.light!.color = UIColor(white: 0.2, alpha: 1.0)
        coinScene.rootNode.addChildNode(ambientLightNode)
        
        omniLightNode.light = SCNLight()
        omniLightNode.light!.type = SCNLightTypeOmni
        omniLightNode.light!.color = UIColor(white: 0.75, alpha: 1.0)
        omniLightNode.position = SCNVector3Make(0, 0, 40)
        coinScene.rootNode.addChildNode(omniLightNode)
        
        let shinyCoinMaterial = SCNMaterial()
//        shinyCoinMaterial.diffuse.contents = UIColor(red: 0.8, green: 0.8, blue: 1.0, alpha: 1.0)
        shinyCoinMaterial.diffuse.contents = UIColor.whiteColor()
        shinyCoinMaterial.specular.contents = UIColor.whiteColor()
        shinyCoinMaterial.transparent.contents = UIImage(named:"glassMap")!
        shinyCoinMaterial.transparency = 0.8
        shinyCoinMaterial.transparencyMode = .RGBZero
        shinyCoinMaterial.shininess = 1.0
//        shinyCoinMaterial.fresnelExponent = 0.9
        shinyCoinMaterial.litPerPixel = true
        square.firstMaterial = shinyCoinMaterial
        
        scene = coinScene
    }
    
    func moveLightSourceAlongVector(vector: SCNVector3) {
        omniLightNode.position = vector
    }
}